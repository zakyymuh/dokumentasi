<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cook extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->library('input');
		$this->load->helper('cookie','input');
	}

	/* 	Note
	*	make dan style nanti disimpan di db
	*	input konten rencananya akan menyesuaikan isi dari make
	*/
	
	public function a()
	{	
		$cookie = array(
		        'name'   => 'TheCookieName',
		        'value'  => 'The Value',
		        'expire' => '86500',
		        // 'domain' => '.some-domain.com',
		        'path'   => '/',
		        'prefix' => 'myprefix_',
		        'secure' => TRUE
		);

		set_cookie($cookie);
	}

	public function b() {
       $a = get_cookie('pre');
       var_dump($a);
   }

   	public function c(){
   		ifdelete_cookie('pre');
   	}

}
