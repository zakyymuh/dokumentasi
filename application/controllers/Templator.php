<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templator extends CI_Controller {

	public function index(){
		$this->load->model('Model_dokumen');
		$data['tipe'] = $this->Model_dokumen->get_many_by(['status'=>'active']);
		
		$this->load->view('templator_view',$data);
	}

	public function submit(){
		if($this->input->post('submit')){
			$data = [
				"dokumen_id" => $this->input->post('tipe'),
				"nama_section" => $this->input->post('nama_section'),
				"content" => html_entity_decode($this->input->post('content'))
			];
			$this->load->model('Model_section');
			$this->Model_section->insert($data);
			echo "<script>alert('Berhasil Membuat dokumen baru')</script>";
			redirect($this->index());
		}else{
			redirect($this->index());
		}

	}

	public function list_section(){
		$this->load->model('Model_section');
		$data['data'] = $this->Model_section->get_all();
		$this->load->view('list_view',$data);
	}

	public function lihat($id = ''){
		$this->load->Model('Model_section');
		$data['content'] = $this->Model_section->get($id);
		$this->load->view('lihat',$data);
	}

	public function list_order(){
		$this->load->model('Model_order');
		$data['result'] = $this->Model_order->get_all();
		$this->load->view('list_order',$data);
	}

	public function d_pdf($id = ''){
		$this->load->library('doc');

		$this->load->Model('Model_section');
		$this->load->model('Model_order');

		$order_data[] = $this->Model_order->get($id);
		$max = $this->Model_section->count_all();
		$data = $this->Model_section->get($max);
		$replace = $this->data_input($data->content,$id);

		$result = "<page>".$replace."</page>";
		$this->load->view('cetak',['content'=>$result]);

	}

	public function d_word($id = ''){
		$this->load->library('doc');

		$this->load->Model('Model_section');
		$this->load->model('Model_order');


		$order_data[] = $this->Model_order->get($id);
		$max = $this->Model_section->count_all();
		$data = $this->Model_section->get($max);
		$replace = $this->data_input($data->content,$id);
		// echo htmlentities($replace,ENT_QUOTES);
		// die();

		$this->load->view('cetak_word',['content'=>$replace]);

	} 

	function data_input($string,$id) {
		$order_data = $this->Model_order->get($id);

		$data = [
			'$id_order' => $order_data->id_order,
			'$id_user' => $order_data->id_user,
			'$gross' => $order_data->gross,
			'$tanggal_order' => $order_data->tanggal_order,
			'data-cke-filler="true"' => '',
		];

		foreach ($data as $key => $value) {
			$string = str_replace($key,$value,$string);
		}

		return $string; 
	}
}