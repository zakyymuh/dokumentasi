<!DOCTYPE html>
<html>
<head>
	<title>LIST VIEW</title>
</head>
<body>    <p>
    	Menu: <a href="<?=base_url();?>templator/">Buat dokumen</a> / <a href="<?=base_url();?>templator/list_section">Daftar Dokumen</a> / <a href="<?=base_url();?>templator/list_order">Daftar Order</a>
    </p>
	<table border=1>
		<tr>
		<th>No</th>
		<th>IDOrder</th>
		<th>IDUser</th>
		<th>Gross</th>
		<th>Tanggal Order</th>
		<th>Download</th>
		</tr>
		<?php $no = 1; ?>
		<?php foreach ($result as $value): ?>
			<tr>
				<td><?=$no?></td>
				<td><?=$value->id_order?></td>
				<td><?=$value->id_user?></td>
				<td><?=$value->gross?></td>
				<td><?=$value->tanggal_order?></td>
				<td>
					<a href="<?=base_url();?>templator/d_pdf/<?=$value->id_order;?>">PDF</a> /
					<a href="<?=base_url();?>templator/d_word/<?=$value->id_order;?>">WORD</a>
			
			</tr>
			<?php $no++; ?>
		<?php endforeach ?>
	</table>
</body>
</html>