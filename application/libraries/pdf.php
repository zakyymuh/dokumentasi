<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class pdf{

		// NOTE
		// A4 page
		// 745
		// 4(186),2(248)

		// -----------------------------
		// |  S1  |  S2  |  S3  |  S4  |
		// -----------------------------
		// |    M1   |   M2   |   M3   |
		// -----------------------------
		// |     L1      |      L2     |
		// -----------------------------
		// |             XL            |
		// -----------------------------
		
		public function __construct(){
        	$this->ci =& get_instance();
    	}

    	public function make(){
        	$data = func_get_args();

        	$content = "<page>";

        	foreach ($data as $value) {
        			$set = $value['make'];
        			$cet = $value['content'];
        	}

        	$set = explode(",",$set);
        	$c_set = count($set);
        	
        	$cnt = explode(",", $cet);

        	for ($i=0; $i < $c_set; $i++) {

        		if(isset($cnt[$i])){
        			$cnt_data = $cnt[$i];
        		}else{
        			$cnt_data = "";
        		}
        		$next = isset($set[$i+1]);
        		if($next){
        			$next_data = $set[$i+1];
        		}
        		
        		$prev = isset($set[$i-1]);
        		if($prev){
        			$prev_data = $set[$i-1];
        		}
        	//S Sukses
        		switch ($set[$i]) {
        			case 'S1':
        				
        				$content .= "<table border=1><tr><td style='width:180px;'>$cnt_data</td>";

        				if($next){

        					if($set[$i+1] == 'S4'){
        						$content .= "<td style='width:180px;'></td><td style='width:180px;'></td>";
        					}elseif($set[$i+1] == 'S3'){
        						$content .= "<td style='width:180px;'></td>";
        					}elseif($set[$i+1] <> 'S2'){
        						$content .= "</tr></table>";
        					}
        				}else{
        					$content .= "</tr></table>";
        				}
        				break;

        			case 'S2':
        				if($prev){
        					if($set[$i-1] <> 'S1'){
        						$content .= "<table border='1'><tr><td style='width:180px;'></td>";
        					}
        				}

        				$content .= "<td style='width:180px;'>$cnt_data</td>";

        				if($next){
        					if($set[$i+1] == 'S4'){
        						$content .= "<td style='width:180px;'></td>";
        					}elseif($set[$i+1] <> 'S3'){
        						$content .= "</tr></table>";
        					}
        				}else{
        					$content .= "</tr></table>";
        				}
        			break;

        			case 'S3':
        				if($prev){
        					if(($prev_data == 'S1') || ($prev_data == 'S2')){
        						// $content .= "<table border='1'><tr><td style='width:180px;'></td><td style='width:180px;'></td>";
        					}else{
        						$content .= "<table border='1'><tr><td style='width:180px;'></td><td style='width:180px;'></td>";
        					}
        				}

        				$content .= "<td style='width:180px;'>$cnt_data</td>";

        				if($next){
        					if($next_data == 'S4'){
        						// $content .= "</tr></table>";
        					}else{
        						$content .= "</tr></table>";
        					}
        				}else{
        					$content .= "</tr></table>";
        				}

        				break;

        			case 'S4':
        				if($prev){
        					if(($prev_data == 'S1') || ($prev_data == 'S2') || ($prev_data == 'S3')){
        					}else{
        						$content .= "<table border='1'><tr><td style='width:180px;'></td><td style='width:180px;'></td><td style='width:180px;'></td>";
        					}
        				}else{
        						$content .= "<table border='1'><tr><td style='width:180px;'></td><td style='width:180px;'></td><td style='width:180px;'></td>";
        				}
        				$content .= "<td style='width:180px;'>$cnt_data</td></tr></table>";
        				break;

        			case 'M1':
        				$content .= "<table border=1><tr><td style='width:242px;'>M1</td>";

        				if($next){
        					if($next_data == "M3"){
        						$content .= "<td style='width:242px;'></td>";
        					}elseif($next_data <> "M2"){
        						$content .= "</tr></table>";
        					}
        				}else{
        						$content .= "</tr></table>";
        				}
        				break;

        			case 'M2':
        				if($prev){
        					if($prev_data <> 'M1'){
        						$content .= "<table border=1><tr><td style='width:242px;'></td>";
							}
        				}
        				$content .= "<td style='width:242px;'>$cnt_data</td>";

        				if($next){
        					if($next_data <> "M3"){
        						$content .= "</tr></table>";
        					}
        				}else{
        						$content .= "</tr></table>";
        				}

        				break;

        			case 'M3':
        				if($prev){
        					if($prev_data == 'M1'){
							}elseif($prev_data == 'M2'){
							}else{
        						$content .= "<table border=1><tr><td style='width:242px;'></td><td style='width:242px;'></td>";
							}
        				}else{
        						$content .= "<table border=1><tr><td style='width:242px;'></td><td style='width:242px;'></td>";
        				}
        					$content .= "<td style='width:242px;'>$cnt_data</td></tr></table>";
        				
        				# code...
        				break;

        			case 'L1':
        				$content .= "<table border=1><tr><td style='width:368px;'>$cnt_data</td>";
        				if($next){
        					if($next_data == "L2"){

        					}else{
        						$content .= "</tr></table>";
        					}
        				}else{
        						$content .= "</tr></table>";
        				}
						break;
        			case 'L2':
        				if($prev){
        					if($prev_data == "L1"){

        					}else{
        						$content .= "<table border=1><tr><td style='width:368px;'></td>";
        					}
        				}
        				$content .= "<td style='width:368px;'>$cnt_data</td></tr></table>";

        				# code...
        				break;
        			case 'XL':
        				$content .= "<table border=1><tr><td style='width:745px;'>$cnt_data</td></tr></table>";
						break;
        		} //end switch		
        		

        	} //end foreach
        	$content .= "</page>";
        	return $content;
    	}
}
?>