-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Nov 2018 pada 16.38
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fasheshop_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen`
--

CREATE TABLE `dokumen` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokumen`
--

INSERT INTO `dokumen` (`id`, `nama`, `status`) VALUES
(1, 'Tiket', 'active'),
(2, 'Struk PLN', 'active'),
(3, 'Tambahan メイソウ', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `section`
--

CREATE TABLE `section` (
  `id` int(11) UNSIGNED NOT NULL,
  `dokumen_id` int(11) UNSIGNED DEFAULT NULL,
  `nama_section` varchar(60) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `section`
--

INSERT INTO `section` (`id`, `dokumen_id`, `nama_section`, `content`) VALUES
(1, 1, 'DokumenQ', '<p>and this is the content of the file</p><p>I will show you a demo.</p><p>this is the data that can fetch easily</p><p>just write down the code</p>$gross'),
(2, 1, 'DokumenBeneran', '<p>Dear User,<br> Disini kami akan sertakan sebuah surat pernyataan bahwa anda telah memesan melalui jasa kami. Terimakasih banyak dan sama-sama.</p>'),
(3, 1, 'DokumenLain', '<p>Ini adalah paragraf sederhana milih kita bersama</p>'),
(4, 1, 'DokumenBaja', '<p>Content</p>'),
(5, 1, '', '<p>ab</p>'),
(6, 1, 'Dokumen ke 20', '<p>Bambang hartanto company adalah sebuah</p><p>hehe</p><p>kepo</p><p>mampus</p>'),
(7, 1, 'nama dokumen', '<p><br data-cke-filler=\"true\"></p>'),
(8, 1, 'nama dokumen', '<p><br data-cke-filler=\"true\"></p>'),
(9, 1, 'nama dokumen', '<p><br data-cke-filler=\"true\"></p>'),
(10, 1, 'Zaky kuy', '<p>Hello kuy barudak whats up?<br><br><br><br><br data-cke-filler=\"true\"></p><p><br data-cke-filler=\"true\"></p><p>$id</p><p><br data-cke-filler=\"true\"></p><p>footer da</p>'),
(11, 1, 'akhir', '<p>Ini adalah halaman untuk percobaan</p><p><br data-cke-filler=\"true\"></p><p>Order id = $id_order</p><p>Id user = $id_user</p><p>Dengan jumlah pembelian sebesar = Rp. $gross</p><p>Pada tanggal: $tanggal_order</p><p><br data-cke-filler=\"true\"></p><p>Terima kasih atas pembeliannya</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id_barang` varchar(6) NOT NULL,
  `id_user` int(3) DEFAULT NULL,
  `nama_barang` varchar(40) DEFAULT NULL,
  `image` text NOT NULL,
  `harga` int(10) DEFAULT NULL,
  `stok` int(4) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`id_barang`, `id_user`, `nama_barang`, `image`, `harga`, `stok`, `deleted`) VALUES
('BR0001', 1, 'Eiger Denim Truck Caps', 'https://eigeradventure.com/media/catalog/product/cache/1/thumbnail/1100x1385/9df78eab33525d08d6e5fb8d27136e95/1/_/1_363_46.jpg', 75000, 10, 0),
('BR0002', 1, 'Jaket SNAKE', 'https://manage.schofficial.com/assets/images/product/b.terry-ph-snake-royal-blue-2.JPG', 350000, 29, 0),
('BR0003', 1, 'Banana Bag', 'http://www.livingandgiving.co.nz/productimages/medium/1/1258_1258_230866.jpg', 50000, 2, 0),
('BR0004', 2, 'Strawberry Bag', 'https://www.collectif.co.uk/images/strawberry-bamboo-bag-p7387-361464_image.jpg', 54000, 2, 0),
('BR0005', 3, 'Batman Bag Premium', 'https://asset1.cxnmarksandspencer.com/is/image/mands/SD_04_T92_6884B_Y4_X_EC_0?$PDP_MAIN_LARGE$', 55000, 34, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id_berita` varchar(8) NOT NULL,
  `judul` varchar(60) NOT NULL,
  `isi` text NOT NULL,
  `tanggal_upload` datetime NOT NULL,
  `tanggal_update` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `id_user` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_berita`
--

INSERT INTO `tb_berita` (`id_berita`, `judul`, `isi`, `tanggal_upload`, `tanggal_update`, `deleted`, `id_user`) VALUES
('BRT18001', 'Hello World', '<p>please welcome ma first post of news.<br></p>', '2018-08-24 09:55:03', '2018-08-24 09:56:10', 0, 1),
('BRT18002', '123123123123', '<p>123123123<br></p>', '2018-08-27 13:12:13', '2018-08-27 13:12:13', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` varchar(6) NOT NULL,
  `id_user` int(3) DEFAULT NULL,
  `gross` int(10) DEFAULT NULL,
  `tanggal_order` datetime NOT NULL,
  `status_code` enum('200','201','406') NOT NULL DEFAULT '200'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_order`
--

INSERT INTO `tb_order` (`id_order`, `id_user`, `gross`, `tanggal_order`, `status_code`) VALUES
('X18040', 1, 210000, '2018-08-31 11:14:39', '200'),
('X18041', 1, 0, '2018-08-31 11:17:03', '200'),
('X18042', 1, 210000, '2018-08-31 11:17:37', '200'),
('X18043', 1, 115000, '2018-08-31 13:17:25', '200'),
('X18044', 1, 210000, '2018-08-31 13:18:09', '200'),
('X18045', 1, 350000, '2018-09-03 10:59:37', '200'),
('X18046', 1, 55000, '2018-09-03 11:07:31', '200'),
('X18047', 1, 150000, '2018-09-03 11:13:21', '200'),
('X18048', 1, 50000, '2018-09-03 11:14:20', '200'),
('X18049', 1, 75000, '2018-09-03 11:15:21', '200'),
('X18050', 1, 50000, '2018-09-03 11:16:12', '200'),
('X18051', 1, 54000, '2018-09-03 11:18:39', '200'),
('X18052', 1, 50000, '2018-09-04 08:45:31', '200'),
('X18053', 1, 54000, '2018-09-04 08:46:39', '200'),
('X18054', 9, 54000, '2018-09-05 11:04:29', '200'),
('X18055', 2, 425000, '2018-09-27 11:38:34', '200'),
('X18056', 2, 350000, '2018-09-27 11:45:14', '200'),
('X18060', 2, 350000, '2018-09-27 13:27:21', '200'),
('X18061', 2, 54000, '2018-09-27 14:23:02', '200'),
('X18063', 2, 50000, '2018-09-27 14:29:30', '200'),
('X18064', 2, 350000, '2018-09-27 14:46:03', '200'),
('X18067', 2, 50000, '2018-09-27 14:50:03', '200'),
('X18068', 2, 75000, '2018-09-27 16:44:41', '200'),
('X18069', 2, 130000, '2018-09-27 16:47:41', '200'),
('X18070', 2, 350000, '2018-09-28 16:30:30', '201');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `id_order_detail` varchar(6) NOT NULL,
  `id_order` varchar(6) NOT NULL,
  `id_barang` varchar(6) NOT NULL,
  `qty` int(3) NOT NULL,
  `price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_order_detail`
--

INSERT INTO `tb_order_detail` (`id_order_detail`, `id_order`, `id_barang`, `qty`, `price`) VALUES
('D18001', 'X18042', 'BR0002', 1, 210000),
('D18002', 'X18043', 'BR0003', 1, 115000),
('D18003', 'X18044', 'BR0002', 1, 210000),
('D18004', 'X18045', 'BR0002', 1, 350000),
('D18005', 'X18046', 'BR0005', 1, 55000),
('D18006', 'X18047', 'BR0001', 2, 75000),
('D18007', 'X18048', 'BR0003', 1, 50000),
('D18008', 'X18049', 'BR0001', 1, 75000),
('D18009', 'X18050', 'BR0003', 1, 50000),
('D18010', 'X18051', 'BR0004', 1, 54000),
('D18011', 'X18052', 'BR0003', 1, 50000),
('D18012', 'X18053', 'BR0004', 1, 54000),
('D18013', 'X18054', 'BR0004', 1, 54000),
('D18014', 'X18055', 'BR0001', 1, 75000),
('D18015', 'X18055', 'BR0002', 1, 350000),
('D18016', 'X18056', 'BR0002', 1, 350000),
('D18017', 'X18061', 'BR0004', 1, 54000),
('D18018', 'X18068', 'BR0001', 1, 75000),
('D18019', 'X18069', 'BR0001', 1, 75000),
('D18020', 'X18069', 'BR0005', 1, 55000),
('D18021', 'X18070', 'BR0002', 1, 350000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id_pesan` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `message` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pesan`
--

INSERT INTO `tb_pesan` (`id_pesan`, `name`, `email`, `phone_number`, `message`) VALUES
(6, 'zaky muhammad yopi rusyana', 'zakyymuh123@gmail.com', '085524421947', 'hello, aku suka banget sama website kamu, asli');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(3) NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(172) NOT NULL,
  `role` enum('pembeli','admin') NOT NULL DEFAULT 'pembeli',
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `email`, `password`, `role`, `deleted`) VALUES
(1, 'zmyr', 'zakyymuh123@gmail.com', 'b8b745f75c8af5d2145b9588ae2d236a0c6a39daccde67d30eb624f793ca6b52b942b903a8627064c37624e5e648cb57583708cf4d882c3e1ee8668c460e2a7dPJtDEPh3ZyUqIIERMp/MhsMgg1+nSmaB1J1t656XYME=', 'admin', 0),
(2, '123', '123@gmail.com', '96169e4ed7bdf0b2458689b123aac0dbcab4971e955b95512941d928532e0df9f27314cf4759c15278b0bd8dbc98b1960a47cab1614821bda224aa6223da5175mIwKDQTIcQI4DwZvfKJZywE/6e1ssjYM6wF0/NwYqMs=', 'pembeli', 0),
(3, '123123', '123@gmail.com', '83b6af51f0aa13ae1aa1feb9ce38995d502ecfd69826d81a541c0f0a7287ad0a8fe68078dae9b7ef3dcb922833e8ae7026afca95f8bf332e36718cf4617df553UcHQ8+xghnrfM1g9AN2/CiuBqyTy0Jc/mmFrWw2tmPE=', 'pembeli', 0),
(9, 'qwer', 'qwer@gmail.com', '6f960be471ba5e170804633a2b783c3c34a0811ec1d027c9fa405808bbf39ab869d4193b221f4a50f260010f4d9bde984106850c310fa535542e0fb2939005bbdYf064mFsCQsF40okzdRPiDQlN2lC+5NWcDkYNBWWuI=', 'pembeli', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dokumen_id` (`dokumen_id`);

--
-- Indeks untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indeks untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`dokumen_id`) REFERENCES `dokumen` (`id`);

--
-- Ketidakleluasaan untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD CONSTRAINT `tb_barang_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`),
  ADD CONSTRAINT `tb_barang_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD CONSTRAINT `tb_berita_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  ADD CONSTRAINT `tb_order_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`),
  ADD CONSTRAINT `tb_order_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD CONSTRAINT `tb_order_detail_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`),
  ADD CONSTRAINT `tb_order_detail_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `tb_barang` (`id_barang`),
  ADD CONSTRAINT `tb_order_detail_ibfk_3` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`),
  ADD CONSTRAINT `tb_order_detail_ibfk_4` FOREIGN KEY (`id_barang`) REFERENCES `tb_barang` (`id_barang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
